import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MongoDB {
    private static ArrayList<Document> documentsList = new ArrayList<>();
    private static ArrayList<Object> documentscomandes = new ArrayList<Object>();
    private static MongoClient mongoClient;
    private static MongoDatabase database;
    private static MongoCollection<Document> collection;


    static {
        mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        database = mongoClient.getDatabase("client");
        collection = database.getCollection("clients");
    }

    public static int menu() {
        System.out.println("Operacions:\n1. Donar d'alta un client\n2.Afegir comandes a un client\n3.Cercar clients per facturació\n4. Cercar clients per quantitat de comandes\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    private static void inserirClientBase() {
        Document document = new Document("nif", "123456789X").append("nom", "Pere Pons").append("total_fact", 0);
        collection.insertOne(document);
    }

    private static void inserirClientBase2(Client c1) {

        Document document = new Document("nif", c1.getNif()).append("nom", c1.getNom()).append("total_fact", c1.getTotal_fact());
        try {
            if (!c1.getCorreu().equals(null)) {
                document.put("correu", c1.getCorreu());
            }
        } catch (Exception e) {
        }
        try {
            if (!c1.getTelf().equals(null)) {
                document.put("telefon", c1.getTelf());
            }
        } catch (Exception e) {
        }
        collection.insertOne(document);
    }

    public static void inserirComanda(Comandes comandes, String nif) {
        Document document = null;
        documentscomandes.clear();
        for (int i = 0; i < documentsList.size(); i++) {
            if (documentsList.get(i).get("nif").equals(nif)) {
                document = documentsList.get(i);
                documentscomandes.add(documentsList.get(i).get("comandes"));
                documentsList.clear();
                System.out.println(documentscomandes);
                   collection.deleteOne(document);
                int maxfact = (int) document.get("total_fact") + comandes.getPreu();
                document.put("total_fact", maxfact);
            }
        }
        documentscomandes.add(new Document("data_com", comandes.getData()).append("import", comandes.getPreu()).append("pagada", comandes.isPagat()));
        System.out.println(documentscomandes.size());
        System.out.println(documentscomandes);
        document.put("comandes", documentscomandes);
        collection.insertOne(document);

    }

    public static void inserirList() {
        for (Document cur : collection.find()) {
            documentsList.add(cur);
            System.out.println(cur.get("nif"));
        }
    }

    private static void llistarClientNif() {
        MongoCursor<Document> cursor = collection.find().iterator();
        try {
            int count = 1;
            while (cursor.hasNext()) {
                System.out.println("Client num: " + count + " nif: " + cursor.next().get("nif"));
                count++;
            }
        } finally {
            cursor.close();
        }
    }

    private static void llistarClientNifFact(int total) {
        for (Document cur : collection.find()) {
            int facturacio = (int) cur.get("total_fact");
            if (facturacio > total) {
                System.out.println(cur.toJson());
            }
        }

    }

    public static void llistarclientComand(int cant) {
        inserirList();
        documentscomandes.clear();
        for (int i = 0; i <documentsList.size() ; i++) {
            int count=0;
            for (Document document : collection.find()) {
                if (!documentsList.get(i).get("comandes").toString().isEmpty()){
                    count++;
                    if (count>cant){
                        System.out.println(document.toJson());
                    }
                }

            }
        }
    }

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    Client client = null;
                    System.out.println("Introdueix les dades d'un client");
                    System.out.println("Nif");
                    String nif = sc.nextLine();
                    System.out.println("Nom");
                    String nom = sc.nextLine();
                    System.out.println("Total Facturacio (sense decimals)");
                    int total_fact = sc.nextInt();
                    System.out.println("Correu electronic");
                    sc.nextLine();
                    String correu = sc.nextLine();

                    System.out.println("Telefon");
                    String telefon = sc.nextLine();
                    client = new Client(nif, nom, total_fact);
                    if (!correu.isEmpty()) {
                        client.setCorreu(correu);
                    }
                    if (!telefon.isEmpty()) {
                        client.setTelf(telefon);
                    }
                    inserirClientBase2(client);
                    break;
                case 2:
                    inserirList();
                    System.out.println("Escolleix un nif per afegir comanda");
                    nif = sc.next();
                    System.out.println("Afegirem la comanda a aquest client");
                    System.out.println("Data de la comanda (dd/mm/yyyy)");
                    String date = sc.next();
                    System.out.println("Preu de la comanda (sense decimals)");
                    int preu = sc.nextInt();
                    System.out.println("Esta pagada?(true/false)");
                    boolean paid = sc.nextBoolean();
                    Comandes comandes = new Comandes(date, preu, paid);
                    inserirComanda(comandes, nif);                    break;
                case 3:
                    System.out.println("Introdueix la facturacio  a cercar");
                    int cant=sc.nextInt();
                    llistarClientNifFact(cant);

                    break;
                case 4:
                    System.out.println("Introdueix la cantitat de comandes a cercar");
//                    cant=sc.nextInt();
                    llistarclientComand(1);


                    break;


            }

            opcio = menu();
        }
    }


}


//documentsList.add(new Document("data_com","12/12/2020").append("import",12).append("pagada",true));
//        documentsList.add(new Document("data_com","22/12/2020").append("import",15).append("pagada",false));
//        documentsList.add(new Document("data_com","4/12/2020").append("import",1).append("pagada",true));
//
//        Document doc=new Document("nif","178945").append("nom","pau").append("total_fact",45).append("telefon",679789456).
//        append("correu","algo@gmail.com").append("comandes",documentsList);
//        collection.insertOne(doc);


