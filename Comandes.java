import java.util.Date;

public class Comandes {
    private String data;
    private int preu;
    private boolean pagat;

    public Comandes(String data, int preu, boolean pagat) {
        this.data = data;
        this.preu = preu;
        this.pagat = pagat;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getPreu() {
        return preu;
    }

    public void setPreu(int preu) {
        this.preu = preu;
    }

    public boolean isPagat() {
        return pagat;
    }

    public void setPagat(boolean pagat) {
        this.pagat = pagat;
    }

    @Override
    public String toString() {
        return "Comandes{" +
                "data=" + data +
                ", preu=" + preu +
                ", pagat=" + pagat +
                '}';
    }
}
