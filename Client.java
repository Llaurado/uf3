public class Client {
    private String nif,nom;

    private int total_fact;
    String correu,telf;

    public Client(String nif, String nom, int total_fact, String correu, String telf) {
        this.nif = nif;
        this.nom = nom;
        this.total_fact = total_fact;
        this.correu = correu;
        this.telf = telf;
    }

    public Client(String nif, String nom, int total_fact) {
        this.nif = nif;
        this.nom = nom;
        this.total_fact = total_fact;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCorreu() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu = correu;
    }

    public String getTelf() {
        return telf;
    }

    public void setTelf(String telf) {
        this.telf = telf;
    }

    public int getTotal_fact() {
        return total_fact;
    }

    public void setTotal_fact(int total_fact) {
        this.total_fact = total_fact;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", correu='" + correu + '\'' +
                ", telf='" + telf + '\'' +
                ", total_fact=" + total_fact +
                '}';
    }
}
